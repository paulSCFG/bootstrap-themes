(function ($) {

$(document).ready(function () {

  loadGoogleFonts();
  loadDataTable();

  // body background color
  changeBackground("#bg-color", "body");
  // body fonts
  changeFont("#bd-font", "body");
  
  // body font size
  changeFontSize("#bg-fontSize", "body");

  // nav background color
  changeBackground("#nav-color", "nav.navbar");
  //nav fonts
  changeFont("#nav-font", "nav.navbar");
  // nav font color
  changeFontColor("#nav-fontColor", "nav.navbar");


  function changeFont(el, targetEl) {
    $(el).on("change", function () {
      var f = $(this).val().replace(" ", "+");
      var link = $("<link href='http://fonts.googleapis.com/css?family="+f+"' rel='stylesheet' type='text/css'>");
      $('head').append(link);

      $(targetEl).css('font-family', $(this).val());

    });
  }

  function changeBackground(el, targetEl) {
    $(el).on("change", function () {
      var color = $(this).val();
      $(targetEl).css("background-color", color);
    });
  }

  function changeFontColor(el, targetEl) {
    $(el).on("change", function () {
      var color = $(this).val();
      $(targetEl).css("color", color);
    });
  }

  function changeFontSize(el, targetEl) {
    $(el).on("change", function () {
      var size = $(this).val();
      $(targetEl).css("font-size", size);
    });
  }


  function loadGoogleFonts() {
    $.getJSON(window.location.href + "/gfonts.json", function (data) {
      var $sel = $(".googlefonts");
      var $opts;
      data.items.forEach(function (font) {
        $opts = $('<option>').val(font.family).text(font.family);
        $sel.append($opts);
      });
    });
  }

  function loadDataTable() {
    $("#myTable").dataTable();
  }

});
})(jQuery);